#include "pgmlib.h"
#include <math.h>
#include <stdio.h>

int image_interpolation(int n1, double x, double y, int inter_method)
{
  double p, q, tmp;
  int output, nearest_x, nearest_y;
  int floor_x, floor_y;

  switch (inter_method)
  {
  case 1:
    nearest_x = round(x);
    nearest_y = round(y);
    output    = image[n1][nearest_x][nearest_y];
    break;

  case 2:
    floor_x = floor(x);
    floor_y = floor(y);
    p       = x - floor_x;
    q       = y - floor_y;
    tmp     = (1.0 - p) * ((1.0 - q) * image[n1][floor_x][floor_y] +
                       q * image[n1][floor_x][floor_y + 1]) +
          p * ((1.0 - q) * image[n1][floor_x + 1][floor_y] +
               q * image[n1][floor_x + 1][floor_y + 1]);
    output = ( int )round(tmp);
    break;

  default:
    output = 0;
    break;
  }

  return output;
}

void image_displace(int n1, int n2, int t_x, int t_y)
{
  width[n2]  = width[n1];
  height[n2] = height[n1];

  for (int y = 0; y < height[n2]; y++)
  {
    for (int x = 0; x < width[n2]; x++)
    {
      int xx = x - t_x;
      int yy = y - t_y;
      if (xx >= 0 && xx < width[n1] && yy >= 0 && yy < height[n1])
        image[n2][x][y] = image[n1][xx][yy];
      else
        image[n2][x][y] = 0;
    }
  }
}

void image_resize(int n1, int n2, double scale, int inter_method)
{
  width[n2]  = width[n1] * scale;
  height[n2] = height[n1] * scale;

  for (int y = 0; y < height[n2]; y++)
  {
    for (int x = 0; x < width[n2]; x++)
    {
      int xx = x / scale;
      int yy = y / scale;
      if (xx >= 0 && xx < width[n1] && yy >= 0 && yy < height[n1])
        image[n2][x][y] = image_interpolation(1, xx, yy, inter_method);
      else
        image[n2][x][y] = 0;
    }
  }
}

void image_rotate(int n1, int n2, double angle, int inter_method)
{
  double theta = angle * M_PI / 180;
  width[n2]    = width[n1];
  height[n2]   = height[n1];

  int centor_x = width[n1] / 2 + 1;
  int centor_y = height[n1] / 2 + 1;

  for (int y = 0; y < height[n2]; y++)
  {
    for (int x = 0; x < width[n2]; x++)
    {
      int xx =
        (x - centor_x) * cos(theta) - (y - centor_y) * sin(theta) + centor_x;
      int yy =
        (x - centor_x) * sin(theta) + (y - centor_y) * cos(theta) + centor_y;

      if (xx >= 0 && xx < width[n1] && yy >= 0 && yy < height[n1])
        // image[n2][x][y] = image[n1][xx][yy];
        image[n2][x][y] = image_interpolation(1, xx, yy, inter_method);
      else
        image[n2][x][y] = 0;
    }
  }
}


int main()
{
  load_image(0, "./letterT.pgm");
  int x, y;
  printf("平行移動分を入力\n\rx->");
  scanf("%d", &x);
  printf("y->");
  scanf("%d", &y);
  image_displace(0, 1, x, y);
  save_image(1, "moved_image.pgm");

  printf("リサイズ->");
  double scale;
  scanf("%lf", &scale);
  image_resize(0, 2, scale, 2);
  save_image(2, "resized_image.pgm");

  printf("回転角度を入力\n");
  double angle;
  scanf("%lf", &angle);
  image_rotate(0, 3, angle, 2);
  save_image(3, "rotated_image.pgm");

  return 0;
}
